package datastructure;


import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {
    
    private int rows;
    private int colums;
    private CellState[][] grid;

    

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.colums = columns;
        grid = new CellState[rows][columns];
        for (int row = 0; row<rows; row++) {
                Arrays.fill(grid[row], initialState);
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return colums;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copy = new CellGrid(this.rows, this.colums, null);
        for (int row=0; row<rows; row++) {
            for (int col=0; col<colums; col++) {
                copy.grid[row][col] = this.grid[row][col];
            }
        }
        return copy;
    }
    
}
