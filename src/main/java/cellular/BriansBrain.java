package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int row, int columns) {
        currentGeneration = new CellGrid(row, columns, CellState.DEAD);
        initializeCells();
    }


    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
                int r= random.nextInt(3);
				if (r==1) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} 
                else if (r==2) {
					currentGeneration.set(row, col, CellState.DEAD);
				}
                else {
                    currentGeneration.set(row, col, CellState.DYING);
                }

			}
		}  
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int row=0; row<numberOfRows();row++) {
			for (int col=0; col<numberOfColumns();col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration=nextGeneration;        
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState cellState = getCellState(row, col);
        int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
        
        if (cellState==CellState.ALIVE) {
            return CellState.DYING;
        }
        else if (cellState==CellState.DYING) {
            return CellState.DEAD;
        }
        else if (cellState==CellState.DEAD && aliveNeighbors==2) {
            return CellState.ALIVE;
        }
        else {
            return CellState.DEAD;
        }
    }

    private int countNeighbors(int row, int col, CellState state) {
        int stateCount = 0;
		int nRow=numberOfRows();
		int nCol=numberOfColumns();

		for (int i=row-1; i<=row+1; i++) {
			for (int j=col-1; j<=col+1; j++) {
				if (i==row && j==col) 
					continue;
				if (getCellState((i+nRow)%nRow, (j+nCol)%nCol)==state) {
					stateCount++;
				}
			}
		}
		return stateCount;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}
